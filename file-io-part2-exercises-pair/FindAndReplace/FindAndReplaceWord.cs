﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindAndReplace
{
    public static class FindAndReplaceWord
    {
        public static void ReplaceWord()
        {
            string directory = Environment.CurrentDirectory;
           
            Console.WriteLine("Please enter the original file path containing the phrase to be searched");
            string inputFilePath = Console.ReadLine();

            bool fileExists = File.Exists(inputFilePath);

            Console.WriteLine("Please enter the destination file path for your phrase replacement");
            string outputFilePath = Console.ReadLine();

            bool directoryExists = Directory.Exists(outputFilePath);

            Console.WriteLine("What would you like to call your file in it's new destination");
            string newFileName = Console.ReadLine();

            string fileTo = ($"{newFileName}.txt");
            string fullPath = Path.Combine(outputFilePath, fileTo);

           
            if (!directoryExists)
            {
                Console.WriteLine("We did not find the directory you were searching for, please try again");
                ReplaceWord();


            }
            if (!fileExists)
            {
                Console.WriteLine("We did not find the file you were searching for, please try again");
                ReplaceWord();
            }


            Console.WriteLine("Please enter the phrase you would like to search for");
            string phraseSearch = Console.ReadLine();

            Console.WriteLine("Please enter the phrase you would like to replace it with");
            string phraseReplace = Console.ReadLine();

            
           

            using (StreamReader sr = new StreamReader(inputFilePath))
            {
                
                using (StreamWriter sw = new StreamWriter(fullPath, false))
                {
                                    
                    while (!sr.EndOfStream)
                    {
                        
                        string line = sr.ReadLine();
                        if (line.Contains(phraseSearch))
                        {                           
                            string fixedLine = line.Replace(phraseSearch, phraseReplace);
                            sw.WriteLine(fixedLine);
                        }
                        
                        
                    }
                }
            }

            Console.WriteLine("Good job buddy, check your file, it's there");







        }

    }
}
