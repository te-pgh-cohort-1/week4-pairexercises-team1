﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace file_io_part1_exercises_pair
{
    public static class FilePaths
    {        

        public static void ReadCharacterFile()
        {
            
            string directory = Environment.CurrentDirectory;
            string filename = @"Book\alices_adventures_in_wonderland.txt";

            string fullPath = Path.Combine(directory, filename);
            

            try
            {
                
                using (StreamReader sr = new StreamReader(fullPath))
                {
                    
                    while (!sr.EndOfStream)
                    {
                        
                        string line = sr.ReadToEnd();

                        
                        string[] words = line.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                        string[] sentences = line.Split('.', '?', '!');

                        
                        int numberOfWords = words.Length;
                        int numberOfSentences = sentences.Length;
                        Console.WriteLine("Please enter the path file name:");
                        Console.ReadLine();
                        Console.WriteLine($"Number of words is: {numberOfWords}");
                        Console.WriteLine($"Number of sentences is: {numberOfSentences}");

                    } 
                }
            }
            catch (IOException e) 
            {
                Console.WriteLine("Error reading the file");
                Console.WriteLine(e.Message);
            }

        }
        
        
    }
}
