﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercises
{
    public class StringCalculator
    {
        int newNum = 0;
        int sum = 0;

        public int Add(string numbers)
        {
            if(numbers.Length == 0)
            {
                return 0;
            }
            else if(numbers.Length == 1)
            {
                int newNum = int.Parse(numbers);
                return newNum;
            }
            else if (numbers.Length > 1)
            {
                char[] delimeters = { ',', '\n' };
                string[] newNumbers = numbers.Split(delimeters);

                foreach (string num in newNumbers)
                {
                    sum += int.Parse(num);
                }
                return sum;
            }
            return 0;
        }
    }
}
