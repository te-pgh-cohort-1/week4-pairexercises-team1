﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercises;
using System;
using System.Collections.Generic;

namespace Exercises.Test
{
    [TestClass]
    public class NumberToWordsTests
    {
        NumberToWords loops = new NumberToWords();

        [TestMethod]
        public void SingleDigit()
        {
            int num = 1;
            string result = loops.NumberWords(num);
            Assert.AreEqual("one", result, "Should be one");
        }

        [TestMethod]
        public void DoubleDigit()
        {
            int num = 23;
            string result = loops.NumberWords(num);
            Assert.AreEqual("twenty-three", result, "Should be twenty-three");
        }
        [TestMethod]
        public void ThreeDigit()
        {
            int num = 345;
            string result = loops.NumberWords(num);
            Assert.AreEqual("three hundred and forty-five", result, "Should be three hundred and forty-five");

        }
        [TestMethod]
        public void FourDigits()
        {
            int num = 5677;
            string result = loops.NumberWords(num);
            Assert.AreEqual("five thousand and six hundred and seventy-seven", result, "Shoud be five thousand and six hundred and seventy seven");

        }
        [TestMethod]
        public void FiveDigit()
        {
            int num = 77345;
            string result = loops.NumberWords(num);
            Assert.AreEqual("seventy-seven thousand and three hundred and forty-five", result, "seventy-seven thousand and three hundred and forty-five");

        }
        [TestMethod]
        public void SixDigits()
        {
            int num = 177345;
            string result = loops.NumberWords(num);
            Assert.AreEqual("one hundred and seventy-seven thousand and three hundred and forty-five", result, "one hundred and seventy-seven thousand and three hundred and forty-five");
        }
        [TestMethod]
        public void HugeNumbers()
        {
            int num = 999999;
            string result = loops.NumberWords(num);
            Assert.AreEqual("nine hundred and ninety-nine thousand and nine hundred and ninety-nine", result, "ninety hundred and ninety-nine thousand and nine hundred and ninety-nine");
        }
        [TestMethod]
        public void MinusTest()
        {
            int num = -67;
            string result = loops.NumberWords(num);
            Assert.AreEqual("minus sixty-seven", result, "Should return minus sixty-seven");
        }
    }
}
