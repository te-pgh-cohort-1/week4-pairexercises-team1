﻿using System;
using System.Collections.Generic;
using System.Text;
using Exercises;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Test
{
    [TestClass]
    public class WordsToNumbersTests
    {
        WordsToNumbers loops = new WordsToNumbers();
        [TestMethod]
        public void ZeroTest()
        {
            string words = "zero";
            int result = loops.WordNumbers(words);
            Assert.AreEqual(0, result, "Should be 0");            
        }

        [TestMethod]
        public void SingleDigit()
        {
            string words = "nine";
            int result = loops.WordNumbers(words);
            Assert.AreEqual(9, result, "Should be 9");
        }
        [TestMethod]
        public void MinusCase()
        {
            string words = "minus seven";
            int result = loops.WordNumbers(words);
            Assert.AreEqual(-7, result, "Should be -7");
        }
        [TestMethod]
        public void DoubleDigit()
        {
            string words = "seventy-seven";
            int result = loops.WordNumbers(words);
            Assert.AreEqual(77, result, "Should be 77");
        }
        [TestMethod]
        public void ThreeDigit()
        {
            string words = "three and seventy-seven";
            int result = loops.WordNumbers(words);
            Assert.AreEqual(377, result, "Should be 377");
        }
        [TestMethod]
        public void FourDigit()
        {
            string words = "four thousand and one hundred and sixty-five";
            int result = loops.WordNumbers(words);
            Assert.AreEqual(4165, result, "Should be 4165");
        }
        [TestMethod]
        public void FiveDigit()
        {
            string words = "fifty-six thousand and two hundred and twenty-one";
            int result = loops.WordNumbers(words);
            Assert.AreEqual(56221, result, "Should be 56221");
        }
        [TestMethod]
        public void SixDigit()
        {
            string words = "seven hundred and fourteen thousand and seven hundred and fourteen";
            int result = loops.WordNumbers(words);
            Assert.AreEqual(714714, result, "Should be 714714");
        }
    }
}
