using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercises;
using System;
using System.Collections.Generic;

namespace Exercises.Test
{
    [TestClass]
    public class StringCalculatorTests
    {
        StringCalculator loops = new StringCalculator();
        //[TestMethod]
        //public void Tests()
        //{                       
        //    string empty = "";
        //    string one = "1";
        //    string two = "2";




        //    int result = loops.Add(empty);
        //    int result1 = loops.Add(one);
        //    int result2 = loops.Add(two);

        //    Assert.AreEqual(0, result, "Exptected empty string");
        //    Assert.AreEqual(1, result1, "Expected string one");
        //    Assert.AreEqual(2, result2, "Expected string two");
        // }

        [TestMethod]
        public void ZeroTest()
        {
            string empty = "";
            int result = loops.Add(empty);
            Assert.AreEqual(0, result, "Exptected empty string");
        }

        [TestMethod]
        public void OneTest()
        {
            string oneMore = "1";
            int result = loops.Add(oneMore);
            Assert.AreEqual(1, result, "Exptected empty string");
        }

        [TestMethod]
        public void OneMoreTest()
        {
            string oneMore = "1,2";
            int result = loops.Add(oneMore);
            Assert.AreEqual(3, result, "Exptected empty string");
        }

        [TestMethod]
        public void NewLineTest()
        {
            string newLine = "1\n2,3";
            int result = loops.Add(newLine);
            Assert.AreEqual(6, result, "Exptected empty string");
        }

    }
}
